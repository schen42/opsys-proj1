/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/
#ifndef _PROCESS_H_
#define _PROCESS_H_
#include <string>
class Process
{
private:
    unsigned int process_id;
    unsigned int burst_time;
    unsigned int start_time; //AKA arrival time
    unsigned int end_time;
    unsigned int first_call_time;
    unsigned int last_call_time;
    unsigned int time_left;
    unsigned int priority;

public:
    Process(unsigned int _start_time, unsigned int _process_id,
        unsigned int _burst_time);
    //Allow the setting of priority
    Process(unsigned int _start_time, unsigned int _process_id,
        unsigned int _burst_time, unsigned int _priority);

    /* GETTERS */
    unsigned int get_id() { return this->process_id; } const
    unsigned int get_time_left() { return this->time_left; } const
    unsigned int get_first_call_time() { return this->first_call_time; } const
    unsigned int get_last_call_time() { return this->last_call_time; } const
    unsigned int get_start_time() { return this->start_time; } const
    unsigned int get_burst_time() { return this->burst_time; } const
    unsigned int get_priority() { return this->priority; } const

    /* SETTERS */
    void set_first_call_time(int x) { this->first_call_time = x; }
    void set_last_call_time(int x) { this->last_call_time = x; }
    void decrement_time_left() { this->time_left--; }

    /* Get process member values, for debugging purposes */
    void to_string();

    /* Currently unused functions, MARKED FOR DELETION */

    /* Returns true if a process has finished running */
    bool has_completed() const
    {
        if (this->time_left == 0)
            return true;
        return false;
    }

    /*
        Subtract a certain amount of time from the time left.
        Don't allow time left to go negative.
        @param time: Time to subtract
        @return The time the proces actually ran
    */
    unsigned int run_for_time(unsigned int time)
    {
        if (time > time_left)
        {
            int time_ran = time_left;
            this->time_left = 0;
            return time_ran;
        }
        this->time_left -= time;
        return time;
    }
};

#endif