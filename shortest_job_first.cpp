/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/
#include <list>
#include <iterator>
#include <stdio.h>
#include <string.h>
#include <climits>

#include "shortest_job_first.h"
#include "process.h"

void ShortestJobFirst::run(std::list<Process> processes, bool preemptive)
{	
    unsigned int time = 0;
    unsigned int ready_queue_element_count = 0;

    unsigned int processes_created = 0;
    unsigned int processes_finished = 0;
    unsigned int process_count = 0;
    //Turnaround time is the time from when the process first enters the ready queue to when it finishes its CPU burst
    unsigned int min_turnaround_time = UINT_MAX;
    unsigned int max_turnaround_time = 0;
    double average_turnaround_time = 0;
    //Initial wait time is the time from when the process first enters the ready queue to when it first starts processing
    unsigned int min_initial_wait_time = UINT_MAX;
    unsigned int max_initial_wait_time = 0;
    double average_initial_wait_time = 0;
    //Total wait time is the total amount of time the process spent waiting
    unsigned int min_total_wait_time = UINT_MAX;
    unsigned int max_total_wait_time = 0;
    double average_total_wait_time = 0;

    //Simple O(n^2) sorting implemented for now

    std::list<Process>::iterator process_iterator;
    std::list<Process>::iterator process_list_iterator;
    std::list<Process>::iterator ready_queue_iterator;

    process_list_iterator = process_list.begin();
    process_iterator = processes.begin();
    ready_queue_iterator = ready_queue.begin();

    //Copy processes into process_list
    for( process_iterator = processes.begin(); process_iterator != processes.end(); process_iterator++ )
    {
        process_list.push_back( (*process_iterator) );
        process_count++;
    }//end for
    
    process_list_iterator = process_list.begin();

    bool first_process = true;

    Process* current_process = &(*process_list_iterator);

    unsigned int last_process_id = UINT_MAX;

    bool context_switching = false;     //Whether the CPU is currently performing a context switch or not

    int context_switch_counter = 0;

    bool process_entry_flag = false;    //Whether a process has entered the ready queue

    process_iterator = processes.begin();
    while( true )   //While loop continuously runs processes until all processes are complete
    {
        if ( context_switching && context_switch_counter <= 0 )     //If at the end of a context switch
            context_switching = false;
        else if ( context_switching )   //If in the middle of a context switch
            context_switch_counter--;   //Decrement context switch counter

        //Add all processes that start at this time to ready queue in correct order
        //+------------------------------------------------------------+
        for ( process_list_iterator = process_list.begin(); process_list_iterator != process_list.end(); process_list_iterator++ )
        {
            Process temp_process = (*process_list_iterator);
            if ( process_list_iterator == process_list.begin() )
                temp_process = (*process_iterator);

            unsigned int process_start_time = temp_process.get_start_time();            //Current process' start time (arrival time)

            ready_queue_iterator = ready_queue.begin();

            if ( process_start_time == time )   //If process is supposed to enter ready queue at this time
            {
                printf("[time %dms] Process %d created (requires %dms CPU time)\n", time, temp_process.get_id(), temp_process.get_burst_time() );
                processes_created++;
                if ( ready_queue_iterator == ready_queue.end() )    //If there is nothing else in the ready queue
                {
                    ready_queue.push_back( temp_process );  //Simply add to back of ready queue                
                }//end if
                else    //Otherwise, add it in the appropriate place based on burst_time
                {
                    unsigned int process_burst_time = temp_process.get_burst_time();    //Current process' burst time
                    while( true )   //Loop until place in ready queue is found for process ( O(n) for each process )
                    {
                        if ( ready_queue_iterator == ready_queue.end() )    //If search is at end of ready queue
                        {
                            ready_queue.push_back( temp_process );  //Place process at end of ready queue
                            break;
                        }//end if
                        else
                        {
                            unsigned int existing_process_time_left = (*ready_queue_iterator).get_time_left();  //Time left of process in ready queue being compared
                            if ( existing_process_time_left > process_burst_time )      //If new process' burst time is less
                            {
                                if ( !first_process && preemptive && (ready_queue_iterator == ready_queue.begin()) )        //For preemptive sjf, If new process has the lowest burst time
                                {
                                    if ( temp_process.get_burst_time() < (*current_process).get_time_left() )   //if new process' burst time is lower than current process' remaining time
                                        process_entry_flag = true;  //Switch them as soon as possible
                                }//end if

                                ready_queue.insert( ready_queue_iterator, temp_process );   //Insert it in this position
                                break;
                            }
                        }//end if
                        ready_queue_iterator++;     //Check next position of ready queue
                    }//end while
                }//end else
            }//end if
        }//end for
        //+------------------------------------------------------------+

        if ( !context_switching )   //Do process related things only while NOT context switching
        {

            if ( process_entry_flag && preemptive )     //For preemptive sjf, if a process can preempt the current one, do so
            {
                process_entry_flag = false;

                Process temp_proc = Process( 0, -1, 0 );

                last_process_id = (*current_process).get_id();
                temp_proc = (*current_process);                 //Place preempted process in temporary variable
                (*current_process) = ready_queue.front();   //Make first item in ready queue the current process
                ready_queue.pop_front();                    //Remove first item from ready queue
                ready_queue.push_front( temp_proc );    //Place preempted process in front of ready queue

                time++;

                printf("[time %dms] Context switch (swapping out process %d for process %d)\n", time, last_process_id, (*current_process).get_id() );
                //Start the context switch
                context_switching = true;
                context_switch_counter = CONTEXT_SWITCH_OVERHEAD;
            }//end if
            if ( ( first_process || (*current_process).has_completed() ) && ( ready_queue.begin() != ready_queue.end() ) )  //If there is no current process and the ready queue is not empty
            {
                (*current_process) = ready_queue.front();   //Make first item in ready queue the current process
                ready_queue.pop_front();                    //Remove first item from ready queue
                ready_queue_element_count--;

                if ( first_process )
                    first_process = false;
                else
                {
                    printf("[time %dms] Context switch (swapping out process %d for process %d)\n", time, last_process_id, (*current_process).get_id() );
                    //Start the context switch
                    context_switching = true;
                    context_switch_counter = CONTEXT_SWITCH_OVERHEAD;
                }//end if
            }//end if
            else        //Otherwise, process the current process
            {
                if ( (*current_process).get_first_call_time() == UINT_MAX )     //If this is the first time process has been called
                    (*current_process).set_first_call_time( time );

                (*current_process).decrement_time_left();   //Process it one time unit

                if ( (*current_process).has_completed() )   //If current process has finished its burst time
                {
                    unsigned int initial_wait_time = (*current_process).get_first_call_time() - (*current_process).get_start_time() - 1;
                    unsigned int turnaround_time = time - (*current_process).get_start_time();
                    unsigned int total_wait_time = turnaround_time - (*current_process).get_burst_time();
                    printf( "[time %dms] Process %d completed its CPU burst (turnaround time %dms, initial wait time %dms, total wait time %dms)\n", time, (*current_process).get_id(), turnaround_time, initial_wait_time, total_wait_time);
                    last_process_id = (*current_process).get_id();  //Record leaving process ID

                    //Update statistics
                    processes_finished++;
                    //Turnaround time
                    if ( min_turnaround_time == UINT_MAX || min_turnaround_time > turnaround_time )
                        min_turnaround_time = turnaround_time;
                    if ( max_turnaround_time < turnaround_time )
                        max_turnaround_time = turnaround_time;
                    average_turnaround_time += turnaround_time;
                    //Initial wait time
                    if ( min_initial_wait_time == UINT_MAX || min_initial_wait_time > initial_wait_time )
                        min_initial_wait_time = initial_wait_time;
                    if ( max_initial_wait_time < initial_wait_time )
                        max_initial_wait_time = initial_wait_time;
                    average_initial_wait_time += initial_wait_time;
                    //Total wait time
                    if ( min_total_wait_time == UINT_MAX || min_total_wait_time > total_wait_time )
                        min_total_wait_time = total_wait_time;
                    if ( max_total_wait_time < total_wait_time )
                        max_total_wait_time = total_wait_time;
                    average_total_wait_time += total_wait_time;

                }//end if
            }//end else
        }//end if
        
        if ( processes_finished == process_count )  //Exit loop when finished processing all processes
            break;
            
        time++;
    }//end while

    //finish average calculations
    average_turnaround_time = average_turnaround_time / process_count;
    average_initial_wait_time = average_initial_wait_time / process_count;
    average_total_wait_time = average_total_wait_time / process_count;
    
    printf( "Turnaround time: min %d ms; avg %.3f ms; max %d ms\n", min_turnaround_time, average_turnaround_time, max_turnaround_time );
    printf( "Initial wait time: min %d ms; avg %.3f ms; max %d ms\n", min_initial_wait_time, average_initial_wait_time, max_initial_wait_time );
    printf( "Total wait time: min %d m; avg %.3f ms; max %d ms\n", min_total_wait_time, average_total_wait_time, max_total_wait_time );
}//end function run
