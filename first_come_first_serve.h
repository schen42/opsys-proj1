/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/

#ifndef _FIRST_COME_FIRST_SERVE_H
#define _FIRST_COME_FIRST_SERVE_H

#include <climits>
#include <iostream>
#include <list>
#include "constants.h"
#include "process.h"

class FirstComeFirstServe
{
public:
    FirstComeFirstServe();
    void run(std::list<Process> processes);
private:
    unsigned int min_turnaround;
    unsigned int max_turnaround;
    unsigned int total_turnaround;
    unsigned int min_initial_wait;
    unsigned int max_initial_wait;
    unsigned int total_initial_wait;
    unsigned int min_total_wait;
    unsigned int max_total_wait;
    unsigned int total_wait;
    std::list<Process*> queue;
};

#endif
