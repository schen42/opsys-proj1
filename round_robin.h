/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/
#ifndef _ROUND_ROBIN_H
#define _ROUND_ROBIN_H
#include <cassert>
#include <climits>
#include <iostream>
#include <list>
#include <stdio.h>
#include "constants.h"
#include "process.h"

class RoundRobin
{
protected:
    unsigned int min_turnaround;
    unsigned int max_turnaround;
    unsigned int total_turnaround;

    unsigned int min_initial_wait;
    unsigned int max_initial_wait;
    unsigned int total_initial_wait;

    unsigned int min_total_wait;
    unsigned int max_total_wait;
    unsigned int total_wait;

    unsigned int timeslice;
    unsigned int switch_time;
private:
    std::list<Process*> queue;
public:
    /* Constructors */
    /* Default constructor sets timeslice to global/settings var */
    RoundRobin();

    /* Overloaded constructor allows for any timeslice */
    RoundRobin(unsigned int _timeslice);

    /* Update statistics */
    void update_statistics(unsigned int turnaround_time,
        unsigned int initial_wait_time, unsigned int total_wait_time);

    /* Given a list of processes to run, run the Round Robin algo */
    void run(std::list<Process> processes);
};

#endif