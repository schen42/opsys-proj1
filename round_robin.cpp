/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/
#include "round_robin.h"
#include "constants.h"

RoundRobin::RoundRobin()
{
    min_turnaround = min_initial_wait = min_total_wait = UINT_MAX;
    max_turnaround = max_initial_wait = max_total_wait = 0;
    total_turnaround = total_initial_wait = total_wait = 0;
    timeslice = RR_TIMESLICE;
    switch_time = 0;
}

RoundRobin::RoundRobin(unsigned int _timeslice)
{
    min_turnaround = min_initial_wait = min_total_wait = UINT_MAX;
    max_turnaround = max_initial_wait = max_total_wait = 0;
    total_turnaround = total_initial_wait = total_wait = 0;
    timeslice = _timeslice;
    switch_time = 0;
}

void RoundRobin::update_statistics(unsigned int turnaround_time,
    unsigned int initial_wait_time, unsigned int total_wait_time)
{
    this->total_turnaround += turnaround_time;
    this->total_initial_wait += initial_wait_time;
    this->total_wait += total_wait_time;

    //Update min/max turnaround times
    if (turnaround_time < this->min_turnaround)
    {
        this->min_turnaround = turnaround_time;
    }
    if (turnaround_time > this->max_turnaround)
    {
        this->max_turnaround = turnaround_time;
    }

    //Update initial wait time stats
    if (initial_wait_time < this->min_initial_wait)
    {
        this->min_initial_wait = initial_wait_time;
    }
    if (initial_wait_time > this->max_initial_wait)
    {
        this->max_initial_wait = initial_wait_time;
    }

    //Update min/max total wait times
    if (total_wait_time < this->min_total_wait)
    {
        this->min_total_wait = total_wait_time;
    }
    if (total_wait_time > this->max_total_wait)
    {
        this->max_total_wait = total_wait_time;
    }
}

void RoundRobin::run(std::list<Process> processes)
{
    unsigned int current_time = 0;
    unsigned int time_run_this_cycle = 0;
    Process* current_process = NULL;
    unsigned int procs_left = processes.size();
    while ( procs_left > 0 )
    {
        //There will be a context switch if the timeslice has expired (and there is a
        //process on the queue) or if a process has finished running (and there is a process
        //on the queue)
        if (time_run_this_cycle == this->timeslice ||
            (current_process && current_process->get_time_left() == 0))
        {
            //If there is a process currently running and it has completed
            if (current_process && current_process->get_time_left() == 0)
            {
                //NOTE: Turnaround time is the total time it took from start to finish
                unsigned int this_turnaround_time =
                    current_time - current_process->get_start_time();
                //NOTE: Initial wait time is the total time it waited until it actually ran
                unsigned int this_wait_time =
                    current_process->get_first_call_time() - current_process->get_start_time();
                //NOTE: Total wait time is all the time the process was not running
                //INCLUDING initial wait time
                unsigned int this_total_wait_time =
                    current_time - current_process->get_burst_time() - current_process->get_start_time();

                printf("[time %ums] Process %u completed its CPU burst",
                    current_time, current_process->get_id());
                printf("(turnaround time %ums, initial wait time %ums, total wait time %ums)\n",
                    this_turnaround_time,
                    this_wait_time,
                    this_total_wait_time);

                //Collect statistics on completed processes
                update_statistics(this_turnaround_time, this_wait_time, this_total_wait_time);

                procs_left--;
            }

            //Context switch
            time_run_this_cycle = 0;
            switch_time = CONTEXT_SWITCH_OVERHEAD;
            if (queue.size() > 0) //If there is something to swap, swap it
            {
                //Get the next process to run
                Process* next = queue.front();

                //Only indicate context switch if the process isn't done?
                printf("[time %ums] Context switch (swapping out process %u for process %u)\n",
                        current_time, current_process->get_id(), next->get_id());

                //Additionally, only push it back onto the queue if it isn't done.
                if (current_process && current_process->get_time_left() > 0)
                {
                    queue.push_back(current_process);
                }

                //Make the current process the next process
                current_process = next;
                queue.pop_front();

                //If this is the first time the new process has run, update the time
                if (current_process->get_first_call_time() == UINT_MAX)
                {
                    current_process->set_first_call_time(current_time);
                }
            }
        }

        //During every new time unit, check to see if a new process has arrived
        //If a process (or multiple processes) have arrived, add it to the queue if a process
        //is currently running, otherwise, run it. To run a process, we set it as the current
        //process (it is not on the queue)
        std::list<Process>::iterator proc;
        for (proc = processes.begin(); proc != processes.end(); proc++)
        {
            if ((*proc).get_start_time() == current_time)
            {
                printf("[time %ums] Process %u created (requires %ums CPU time)\n",
                    current_time, proc->get_id(), proc->get_burst_time());
                if (current_process)
                {
                    queue.push_back(&(*proc));
                }
                else
                {
                    current_process = (&(*proc));

                    //Also, if this is the first time the process has run, update the time
                    if (current_process->get_first_call_time() == UINT_MAX)
                    {
                        current_process->set_first_call_time(current_time);
                    }
                }
            }
        }

        //At the end, move time forward by one and decrease time left
        //for currently running process (if there is one)
        current_time++;
        if (switch_time == 0)
        {
            time_run_this_cycle++;
            if (current_process) //Decrement time left for current_process
            {
                current_process->decrement_time_left();
            }
        }
        else
        {
            switch_time--;
        }
    }

    //After processes have completed, print the stats
    printf("Turnaround time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_turnaround,
        this->total_turnaround / (double)processes.size(),
        this->max_turnaround);
    printf("Initial wait time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_initial_wait,
        this->total_initial_wait / (double)processes.size(),
        this->max_initial_wait);
    printf("Total wait time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_total_wait,
        this->total_wait / (double)processes.size(),
        this->max_total_wait);
}