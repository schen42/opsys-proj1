/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/
#include <climits>
#include <iostream>
#include "process.h"

Process::Process(unsigned int _start_time, unsigned int _process_id, unsigned int _burst_time)
{
    process_id = _process_id;
    start_time = _start_time;
    time_left = burst_time = _burst_time;
    end_time = 0;
    first_call_time = UINT_MAX; //Basically, set it to any invalid time
    last_call_time = 0;
    priority = 0;
}

Process::Process(unsigned int _start_time, unsigned int _process_id,
        unsigned int _burst_time, unsigned int _priority)
{
    process_id = _process_id;
    start_time = _start_time;
    time_left = burst_time = _burst_time;
    end_time = 0;
    first_call_time = UINT_MAX; //Basically, set it to any invalid time
    last_call_time = 0;
    priority = _priority;
}

void Process::to_string()
{
    std::cout << "---\n" << process_id << "\nStart: " << start_time
        << "\nBurst: " << burst_time
        << "\nPriority:" << priority << std::endl;
}