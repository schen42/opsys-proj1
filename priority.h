#ifndef _PRIORITY_H_
#define _PRIORITY_H_
#include <queue>
#include <vector>
#include "round_robin.h"
/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/

// The comparison class. Higher priority goes first.
class ComparePriority
{
public:
    bool operator() (Process *left, Process *right)
    {
        if (left->get_priority() > right->get_priority())
        {
            return true;
        }
        else if (left->get_priority() == right->get_priority())
        {
            if (left->get_last_call_time() > right->get_last_call_time())
            {
                return true;
            }
        }
        return false;
    }
};

class Priority: public RoundRobin
{
public:
    Priority(unsigned int _timeslice);
    void run(std::list<Process> processes);
private:
    std::priority_queue<Process*, std::vector<Process*>, ComparePriority> queue;
};

#endif