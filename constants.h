/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/

#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#define CONTEXT_SWITCH_OVERHEAD 17

#define NUMBER_OF_PROCESSES 5

#define MIN_BURST_TIME 500
#define MAX_BURST_TIME 4000

//Round-robin time slice
#define RR_TIMESLICE 200

//The average arrival time we want the exponential
//distribution to produce
#define EXP_AVERAGE 1000

//The maximum arrival time we want our exponential distribution to produce
#define EXP_CUTOFF 8000

#endif
