/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/

#include <cmath>
#include <iostream>
#include <list>
#include <stdlib.h>
#include "constants.h"
#include "first_come_first_serve.h"
#include "MersenneTwister.h"
#include "process.h"
#include "round_robin.h"
#include "priority.h"
#include "shortest_job_first.h"

unsigned int rand_exponential(unsigned int average, unsigned int max_value,
    MTRand &rng);

int main(int argc, char* argv[])
{
    bool PART2 = false;
    if (argc == 2 && std::string(argv[1]) == std::string("-PART2"))
    {
        PART2 = true;
    }
    MTRand rng;
    std::list<Process> proc_list;

     //Part 1, all processes start at the same time
    if (!PART2)
    {
        std::cout << "Part 1 (all processes start at time 0):" << std::endl;
        for (unsigned int i = 1; i <= NUMBER_OF_PROCESSES; i++)
        {
            Process temp(0, i,
                MIN_BURST_TIME + rng.randInt(MAX_BURST_TIME - MIN_BURST_TIME),
                rng.randInt(4)
            );
            proc_list.push_back(temp);
        }
    }
    //Enter part 2 code here...20% start at time 0, 80% are random
    else
    {
        std::cout << "Part 2:" << std::endl;
        for (unsigned int i = 1; i <= NUMBER_OF_PROCESSES; i++)
        {
            unsigned int arrival_time = 0;
            if (rng.rand() > 0.2) //20 percent probability or actually force 20%?
            {
                arrival_time = rand_exponential(EXP_AVERAGE, EXP_CUTOFF, rng);
            }
            Process temp(arrival_time, i,
                MIN_BURST_TIME + rng.randInt(MAX_BURST_TIME - MIN_BURST_TIME),
                rng.randInt(4)
            );
            proc_list.push_back(temp);
        }
    }

    // Print all processes (DEBUG)
    // for (std::list<Process>::iterator p = proc_list.begin(); p != proc_list.end(); p++)
    //     p->to_string();
    // std::cout << std::endl;

    if (proc_list.size() < 1)
    {
        //Maybe print out all zeros instead.
        std::cerr << "0 processes" << std::endl;
        return EXIT_FAILURE;
    }

    printf("------------\nFirst Come First Serve\n------------\n");
    FirstComeFirstServe fcfs;
    fcfs.run(proc_list);

    printf("------------\nNon-Preemptive SJF\n------------\n");
    ShortestJobFirst sjf_np;
    sjf_np.run( proc_list, false );

    printf("------------\nPreemptive SJF\n------------\n");
    ShortestJobFirst sjf_p;
    sjf_p.run( proc_list, true );

    printf("------------\nRound Robin\n------------\n");
    RoundRobin rr = RoundRobin(RR_TIMESLICE);
    rr.run(proc_list);

    printf("------------\nPriority\n------------\n");
    Priority pp = Priority(RR_TIMESLICE);
    pp.run(proc_list);

    return EXIT_SUCCESS;
}

/*
    Get a randomly generated variable with an exponential
    distribution ~F(average) truncated to an integer.

    @precondition: average << cutoff
    @param average: The average if we randomly
    generated an infinite number of exponentials
    @return: A random variable with an exponential distribution
*/
unsigned int rand_exponential(unsigned int average, unsigned int max_value,
    MTRand &rng)
{
    //Source: Wikipedia - Exponential Distribution
    //(Generating exponential variates)
    //E[x] = 1/lambda
    //x = -ln(random #) / lambda
    double lambda = 1 / (double)average;
    unsigned int result = abs((unsigned int)(-1 * std::log(rng.rand()) / lambda));
    //Generate another number if the value is above the cutoff
    if (result > max_value)
        return rand_exponential(average, max_value, rng);
    else
        return result;
}
