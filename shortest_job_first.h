/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/
#ifndef _SHORTEST_JOB_FIRST_H
#define _SHORTEST_JOB_FIRST_H

#include <iostream>
#include <list>
#include "constants.h"
#include "process.h"

class ShortestJobFirst
{
private:
    /*//Turnaround time is the time from when the process first enters the ready queue to when it finishes its CPU burst
    unsigned int min_turnaround_time;
    unsigned int max_turnaround_time;
    double average_turnaround_time;
    //Initial wait time is the time from when the process first enters the ready queue to when it first starts processing
    unsigned int min_initial_wait_time;
    unsigned int max_initial_wait_time;
    double average_initial_wait_time;
    //Total wait time is the total amount of time the process spent waiting
    unsigned int min_total_wait_time;
    unsigned int max_total_wait_time;
    double average_total_wait_time;*/

    std::list<Process> process_list;
    std::list<Process> ready_queue;

public:
    ShortestJobFirst()
    {
        //min_turnaround_time = max_turnaround_time = min_initial_wait_time = max_initial_wait_time = average_initial_wait_time
        //  = min_total_wait_time = max_total_wait_time = average_total_wait_time = 0;

        //average_turnaround_time = average_initial_wait_time = average_total_wait_time = 0;
    }

    //Simulate processing of all provided processes using Shortest Job First algorithm
    void run(std::list<Process> processes, bool preemptive);

    //Print appropraite message on process creation
    //void ShortestJobFirst::process_creation_message( unsigned int process_id, unsigned int burst_time );
};

#endif
