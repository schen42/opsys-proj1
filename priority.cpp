/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/
#include "priority.h"

Priority::Priority(unsigned int _timeslice)
{
    min_turnaround = min_initial_wait = min_total_wait = UINT_MAX;
    max_turnaround = max_initial_wait = max_total_wait = 0;
    total_turnaround = total_initial_wait = total_wait = 0;
    timeslice = _timeslice;
}

void Priority::run(std::list<Process> processes)
{
    unsigned int current_time = 0;
    unsigned int time_run_this_cycle = 0;
    unsigned int procs_left = processes.size();
    unsigned int switch_time = 0;
    Process* current_process = NULL;

    while ( procs_left > 0 )
    {
        std::list<Process>::iterator proc;
        for (proc = processes.begin(); proc != processes.end(); proc++)
        {
            if (proc->get_start_time() == current_time)
            {
                printf("[time %ums] Process %u created (requires %ums CPU time)\n",
                    current_time, proc->get_id(), proc->get_burst_time());

                // Push new arrivals onto the queue.
                queue.push(&(*proc));
                if (switch_time == 0 && current_process)
                {
                    // If a new arrival has a higher priority, switch.
                    Process* next = queue.top();
                    if (next != current_process)
                    {
                        printf("[time %ums] Context switch (swapping out process %u for process %u)\n",
                            current_time, current_process->get_id(), next->get_id());
                        current_process = next;
                    }
                    if (current_process->get_first_call_time() == UINT_MAX)
                    {
                        current_process->set_first_call_time(current_time);
                    }
                    switch_time = CONTEXT_SWITCH_OVERHEAD;
                }
                else
                {
                    current_process = queue.top();
                    // Update its time the first time a process is called.
                    if (current_process->get_first_call_time() == UINT_MAX)
                    {
                        current_process->set_first_call_time(current_time);
                    }
                }
            }
        }


        // Switch if time expired or process finished running.
        if (switch_time == 0 && (time_run_this_cycle >= this->timeslice ||
            (current_process && current_process->get_time_left() == 0)))
        {
            // If there is a process currently running and it has completed
            if (current_process && current_process->get_time_left() == 0)
            {
                // Turnaround time is the total time it took from start to finish
                unsigned int this_turnaround_time =
                    current_time - current_process->get_start_time();
                // Initial wait time is the total time it waited until it actually ran
                unsigned int this_wait_time =
                    current_process->get_first_call_time() - current_process->get_start_time();
                // Total wait time is all the time the process was not running
                unsigned int this_total_wait_time =
                    current_time - current_process->get_burst_time() - current_process->get_start_time();

                printf("[time %ums] Process %u completed its CPU burst",
                    current_time, current_process->get_id());
                printf("(turnaround time %ums, initial wait time %ums, total wait time %ums)\n",
                    this_turnaround_time,
                    this_wait_time,
                    this_total_wait_time);

                //Collect statistics on completed processes
                update_statistics(this_turnaround_time, this_wait_time, this_total_wait_time);

                procs_left--;
            }

            // Context switch
            time_run_this_cycle = 0;
            switch_time = CONTEXT_SWITCH_OVERHEAD;
            if (queue.size() > 0)
            {
                // Only push it back onto the queue if it isn't done.
                if (current_process && current_process->get_time_left() > 0)
                {
                    queue.push(current_process);
                }
                queue.pop();
                // Get the next process to run
                Process* next = queue.top();

                if (next != current_process)
                {
                    printf("[time %ums] Context switch (swapping out process %u for process %u)\n",
                            current_time, current_process->get_id(), next->get_id());
                    current_process = next;
                }

                //If this is the first time the new process has run, update the time
                if (current_process->get_first_call_time() == UINT_MAX)
                {
                    current_process->set_first_call_time(current_time);
                }
            }
        }


        // Increment time and run process for a time.
        current_time++;
        if (switch_time == 0)
        {
            time_run_this_cycle++;
            if (current_process)
            {
                current_process->set_last_call_time(current_time);
                current_process->decrement_time_left();
            }
        }
        else
        {
            switch_time--;
        }

    }

    //After processes have completed, print the stats
    printf("Turnaround time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_turnaround,
        this->total_turnaround / (double)processes.size(),
        this->max_turnaround);
    printf("Initial wait time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_initial_wait,
        this->total_initial_wait / (double)processes.size(),
        this->max_initial_wait);
    printf("Total wait time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_total_wait,
        this->total_wait / (double)processes.size(),
        this->max_total_wait);
}
