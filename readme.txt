Type "cmake [source]" without quotations, where [source] is the location of our files.
This creates the makefile.
Type "make" to compile. The -Wall flag is included.
"./project1" to run our program.