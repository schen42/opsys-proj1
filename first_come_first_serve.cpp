/*
    Opsys Team Project 1
    Zev Battad, Sean Chen, Gary Lu
*/

#include "first_come_first_serve.h"
#include <cstdio>

FirstComeFirstServe::FirstComeFirstServe()
{
    min_turnaround = UINT_MAX;
    max_turnaround = 0;
    total_turnaround = 0;
    min_initial_wait = UINT_MAX;
    max_initial_wait = 0;
    total_initial_wait = 0;
    min_total_wait = UINT_MAX;
    max_total_wait = 0;
    total_wait = 0;
}

void FirstComeFirstServe::run(std::list<Process> processes)
{
    unsigned int current_time = 0;
    unsigned int switch_time = 0;

    int n = NUMBER_OF_PROCESSES;
    while (n >= 1)
    {
        //First, push all new arrivals into the queue
        std::list<Process>::iterator arrival_itr;
        for (arrival_itr = processes.begin(); arrival_itr != processes.end(); arrival_itr++)
        {
            if (current_time == arrival_itr->get_start_time())
            {
                std::cout << "[time " << current_time;
                std::cout << "ms] Process " << arrival_itr->get_id();
                std::cout << " created (requires " << arrival_itr->get_burst_time();
                std::cout << "ms CPU time)" << std::endl;
                queue.push_back(&(*arrival_itr));
            }
        }

        //Evaluate the process at the front of the queue.  If it can run, run it.
        if (queue.size() > 0)
        {
            std::list<Process*>::iterator itr = queue.begin();
            Process *process = *itr;

            if (switch_time == 0)
            {
                if (process->get_first_call_time() == UINT_MAX)
                {
                    process->set_first_call_time(current_time);
                }
                process->decrement_time_left();
            }
            else
            {
                switch_time--;
            }

            current_time ++;

            if (process->get_time_left() == 0)
            {
                std::cout << "[time " << current_time << "ms] Process " << process->get_id();

                unsigned int turnaround = current_time - process->get_start_time();
                std::cout << " completed its CPU burst (turnaround time " << turnaround;
                if (turnaround < min_turnaround || min_turnaround == UINT_MAX)
                {
                    min_turnaround = turnaround;
                }
                if (turnaround > max_turnaround)
                {
                    max_turnaround = turnaround;
                }
                total_turnaround += turnaround;

                unsigned int initial_wait = process->get_first_call_time() - process->get_start_time();
                std::cout << "ms, initial wait time " << initial_wait;
                if (initial_wait < min_initial_wait || min_initial_wait == UINT_MAX)
                {
                    min_initial_wait = initial_wait;
                }
                if (initial_wait > max_initial_wait)
                {
                    max_initial_wait = initial_wait;
                }
                total_initial_wait += initial_wait;

                unsigned int total_wait_time = turnaround - process->get_burst_time();
                std::cout << "ms, total wait time " << total_wait_time << "ms)" << std::endl;
                if (total_wait < min_total_wait || min_total_wait == UINT_MAX)
                {
                    min_total_wait = total_wait;
                }
                if (total_wait > max_total_wait)
                {
                    max_total_wait = total_wait;
                }
                total_wait += total_wait_time;

                itr++;
                if (itr != queue.end())
                {
                    std::cout << "[time " << current_time;
                    std::cout << "ms] Context switch (swapping out process " << process->get_id();

                    std::cout << " for process " << (*itr)->get_id();
                    std::cout << ")" << std::endl;
                }

                queue.pop_front();
                n--;
                switch_time = CONTEXT_SWITCH_OVERHEAD;
            }
        }
        else
        {
            current_time++;
        }
    }

    //After processes have completed, print the stats
    printf("Turnaround time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_turnaround,
        this->total_turnaround / (double)processes.size(),
        this->max_turnaround);
    printf("Initial wait time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_initial_wait,
        this->total_initial_wait / (double)processes.size(),
        this->max_initial_wait);
    printf("Total wait time: min %u ms; avg %.3f ms; max %u ms\n",
        this->min_total_wait,
        this->total_wait / (double)processes.size(),
        this->max_total_wait);
}
